package com.learn.springrestwebservice.controller;

import com.learn.springrestwebservice.model.SimpleBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicEndpointController {

    @GetMapping(path = "/hello-world")
    public String helloWorld() {
        return "Hello World";
    }

    @GetMapping(path = "/simple-bean")
    public SimpleBean getSimpleBean() {
        return new SimpleBean("Hello World");
    }

    //path variable send in the URL
    @GetMapping(path = "/simple-bean/path-variable/{name}")
    public SimpleBean helloWorldPathVariable(@PathVariable String name) {
        return new SimpleBean(String.format("Hello World, %s", name));
    }
}
