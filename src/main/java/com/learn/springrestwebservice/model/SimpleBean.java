package com.learn.springrestwebservice.model;

public class SimpleBean {

    private String message;

    public SimpleBean(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("SimpleBean [message=%s]", message);
    }

}

